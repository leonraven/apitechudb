package com.techu.apitechudb.controller;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){

        System.out.println("getProducts");
        ResponseEntity<List<ProductModel>> response =  new ResponseEntity<>(this.productService.findAll(),
                                                                            HttpStatus.OK);
        return response;
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){

        System.out.println("getProductById");
        System.out.println(id);
        Optional<ProductModel> optional = this.productService.findById(id);

        ResponseEntity<Object> response =  new ResponseEntity<>(optional.isPresent() ? optional.get() : "Producto no encontrado",
                                                                optional.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
        return response;
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProducts(@RequestBody ProductModel product){

        System.out.println("addProducts");
        System.out.println(product.getId());
        ResponseEntity<ProductModel> response = new ResponseEntity<>(this.productService.add(product),
                                                                      HttpStatus.CREATED);
        return response;
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("updateProduct");
        System.out.println(product.getId());

        Optional<ProductModel> product2Update = this.productService.findById(id);

        ResponseEntity<ProductModel> response = null;
        if(product2Update.isPresent()){
            response = new ResponseEntity<>(this.productService.update(product, id),
                                                                       HttpStatus.CREATED);
        }else{
            response = new ResponseEntity<>(product,
                                            HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @DeleteMapping("/product/id")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){

        System.out.println("deleteProducts");
        System.out.println(id);
        boolean deleteProduct = this.productService.delete(id);
        ResponseEntity<String> response = new ResponseEntity<>(deleteProduct ? "Producto Borrado" : "Producto no borrado",
                                                               deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
        return response;
    }
}
