package com.techu.apitechudb.controller;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @PostMapping("/purchases")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase){

        System.out.println("addPurchase");
        System.out.println(purchase.getId());

        ResponseEntity<Object> response = null;
        if(this.purchaseService.add(purchase)){
            response = new ResponseEntity<>(purchase,
                    HttpStatus.CREATED);
        }else{
            response = new ResponseEntity<>("No se pudo comprar, por que el usuario o el producto no existe",
                    HttpStatus.NOT_FOUND);
        }

        return response;
    }

}
