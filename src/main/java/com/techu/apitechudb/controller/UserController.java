package com.techu.apitechudb.controller;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){

        System.out.println("addUser");
        System.out.println(user.getId());
        ResponseEntity<UserModel> response = new ResponseEntity<>(this.userService.add(user),
                                                                      HttpStatus.CREATED);
        return response;
    }
    /*@GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(){

        System.out.println("getUsers");
        ResponseEntity<List<UserModel>> response =  new ResponseEntity<>(this.userService.findAll(),
                HttpStatus.OK);
        return response;
    }*/

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsersOrdered(@RequestParam/*(name=orderby, required=false)*/ Optional<String> order){

        System.out.println("getUsersOrdered");
        ResponseEntity<List<UserModel>> response =  new ResponseEntity<>(this.userService.findAll(order),
                                                                         HttpStatus.OK);
        return response;
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){

        System.out.println("getUserById");
        System.out.println(id);
        Optional<UserModel> optional = this.userService.findById(id);

        ResponseEntity<Object> response =  new ResponseEntity<>(optional.isPresent() ? optional.get() : "Usuario no encontrado",
                                                                optional.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
        return response;
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){

        System.out.println("updateUser");
        System.out.println(user.getId());

        Optional<UserModel> user2Update = this.userService.findById(id);

        ResponseEntity<UserModel> response = null;
        if(user2Update.isPresent()){
            response = new ResponseEntity<>(this.userService.update(user, id),
                    HttpStatus.CREATED);
        }else{
            response = new ResponseEntity<>(user,
                                            HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @DeleteMapping("/users/id")
    public ResponseEntity<String> deleteUser(@PathVariable String id){

        System.out.println("deleteUser");
        System.out.println(id);
        boolean deleteUser = this.userService.delete(id);
        ResponseEntity<String> response = new ResponseEntity<>(deleteUser ? "Usuario Borrado" : "Usuario no borrado",
                                                               deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND);
        return response;
    }
}
