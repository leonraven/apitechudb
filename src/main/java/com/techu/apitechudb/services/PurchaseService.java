package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    public boolean add(PurchaseModel purchase){
        System.out.println("add PurchaseService");

        boolean passed = false;
        Optional<UserModel> optionalUser = this.userService.findById(purchase.getUserId());
        //ArrayList<Optional<ProductModel>> productsList = new ArrayList<Optional<ProductModel>>();

        ArrayList<Boolean> productsList = new ArrayList<Boolean>();
        purchase.getPurchaseItems().forEach((key, value) -> productsList.add(this.productService.findById(key).isPresent()));

        System.out.println(productsList.toString());

        if(optionalUser.isPresent() && !productsList.contains(false)){
            this.purchaseRepository.save(purchase);
            passed = true;
        }

        return passed;
    }
}
