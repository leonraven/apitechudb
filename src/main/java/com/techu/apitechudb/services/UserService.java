package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public UserModel add(UserModel user){
        System.out.println("add UserService");
        return this.userRepository.save(user);
    }

    /*
    public List<UserModel> findAll() {
        System.out.println("findAll");
        return this.userRepository.findAll();
    }*/

    public Optional<UserModel> findById(String id) {
        System.out.println("findById");
        return this.userRepository.findById(id);
    }

    public List<UserModel> findAll(Optional<String> order) {
        System.out.println("findAll");
        List<UserModel> listUsers = new ArrayList<>();
        if(order.isPresent()){
            listUsers = this.userRepository.findAll(Sort.by(Sort.Direction.fromString(order.get()), "age"));
        }else{
            listUsers = this.userRepository.findAll();
        }

        return listUsers;
    }

    public UserModel update(UserModel user, String id){
        System.out.println("update UserService");
        user.setId(id);
        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("update UserService");
        boolean result = false;
        if(this.findById(id).isPresent()){
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
